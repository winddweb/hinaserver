class AddCachedToSearchRecords < ActiveRecord::Migration[5.0]
  def change
    add_column :search_records, :cached, :boolean, default: false
    add_column :tweets, :user_id, :string
  end
end
