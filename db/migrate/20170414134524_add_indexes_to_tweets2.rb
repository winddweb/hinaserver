class AddIndexesToTweets2 < ActiveRecord::Migration[5.0]
  def change
    add_index :tweets, %i(source_checked idol_id user_id)
  end
end
