class DropCountViews < ActiveRecord::Migration[5.0]

  def up
    execute <<~SQL
      DROP VIEW count_data
    SQL

    execute <<~SQL
      DROP VIEW extracted_tweets
    SQL
  end

  def down
    execute <<~SQL
      CREATE VIEW extracted_tweets AS
        SELECT
          id,
          idol_id,
          json_extract(json, '$.user.id')                      AS user_id,
          json_extract(json, '$.user.screen_name')             AS screen_name,
          json_extract(json, '$.user.name')                    AS user_name,
          json_extract(json, '$.user.profile_image_url_https') AS profile_image_url,
          json_extract(json, '$.text')                         AS text,
          json_extract(json, '$.created_at')                   AS created_at,
          json_extract(json, '$.source')                       AS source
        FROM tweets
    SQL

    execute <<~SQL
      CREATE VIEW count_data AS
        SELECT 
          idol_id,
          tweet_count,  
          user_count
        FROM idols
          LEFT OUTER JOIN (
            SELECT
              idol_id,
              count(*) AS tweet_count,
              count(DISTINCT user_id) AS user_count
            FROM extracted_tweets
            WHERE source = '<a href="http://sp.pf.mbga.jp/12008305" rel="nofollow">アイドルマスター シンデレラガールズ公式</a>'
            GROUP BY idol_id
          ) tw_counts ON idols.id = tw_counts.idol_id
    SQL
  end

end
