Rails.application.routes.draw do
  root 'top#index'
  get '/system/update', to: 'top#update'

  resources :idols, only: %i(show)
end
