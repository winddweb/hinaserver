Time::DATE_FORMATS[:twitter_search] = '%Y-%m-%d_%H:%M:%S_%Z'
Time::DATE_FORMATS[:local] = '%Y/%m/%d %H:%M'
Time::DATE_FORMATS[:short_md] = '%m/%d'
