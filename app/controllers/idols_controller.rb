class IdolsController < ApplicationController

  def show
    @idol = Idol.find params[:id]

    # meta tags
    set_meta_tags twitter: {
      card: 'summary',
      site: '@tottokotkd',
      title: "全自動でツイート集計したやつ | #{@idol.display_name}",
      description: '15分毎に自動更新するけど何回もアクセスしないで (サーバーが死ぬ)',
    }
  end

  private

  def count_chart
    dates = SearchRecord.start_of_campaign.to_date...SearchRecord.latest_update_timestamp.to_date
    tweets_by_day = dates.map do |date| @idol.tweet_of_date date end
    tweets_until_day = dates.map do |date| @idol.tweet_until_date date end
    users_by_day = dates.map do |date| @idol.user_of_date date end
    users_until_day = dates.map do |date| @idol.user_until_date date end

    LazyHighCharts::HighChart.new('graph') do |f|
      f.title(text: 'ツイート・アカウント数')
      f.xAxis(categories: dates.map{ |d| d.to_time.to_s(:short_md)})
      f.series(name: '1日の増加ツイート数', yAxis: 1, data: tweets_by_day, type: 'column')
      f.series(name: '1日の増加アカウント数', yAxis: 1, data: users_by_day, type: 'column')
      f.series(name: '合計ツイート数', yAxis: 0, data: tweets_until_day)
      f.series(name: '合計アカウント数', yAxis: 0, data: users_until_day)
      f.yAxis(
        [
          { title: { text: '合計' }, opposite: true, min: 0, max: total_maximum },
          { title: { text: '1日あたり'}, margin: 10, min: 0, max: daily_maximum },
        ])
    end
  end

  def tpa_chart
    return nil if @idol.tweet_count == 0

    tpa_data = @idol.tweet_per_user_map
    range = (1..tpa_data.keys.max).to_a

    account_counts = range.
      map{ |n| tpa_data.include?(n) ? tpa_data[n] : 0 }
    account_rates = account_counts.
      map{ |n| n * 100.0 / @idol.user_count }

    tweets_by_account_group = account_counts.map.
      with_index(1) { |account_count, tweet_count| account_count * tweet_count }

    total_tweets = tweets_by_account_group.reduce([]) do |arr, n|
      last = arr.empty? ? 0 : arr.last
      arr.append(last + n)
      arr
    end

    tw_rates = total_tweets.map{ |n| 100.0 * n / @idol.tweet_count }

    LazyHighCharts::HighChart.new('graph') do |f|
      f.title(text: 'ツイート数とアカウント数')
      f.series(name: 'ツイート全体に占める割合', data: tw_rates.map{ |n| n.round(2) }, type: 'areaspline')
      f.series(name: 'アカウント分布', data: account_rates.map{ |n| n.round(2) }, type: 'column')
      f.yAxis(title: { text: '全体に占める割合' }, min: 0, max: 100)
      f.xAxis(title: { text: '同一アカウントによるツイート数' }, margin: 10, categories: range)
    end
  end

  def daily_maximum
    params[:d_max].present? ? params[:d_max].to_i : nil
  end

  def total_maximum
    params[:t_max].present? ? params[:t_max].to_i : nil
  end

  helper_method %i(count_chart tpa_chart daily_maximum total_maximum)

end
