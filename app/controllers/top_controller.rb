class TopController < ApplicationController

  def index
    @idols = Rails.cache.fetch "TopController/index/#{SearchRecord.latest_update_timestamp}/#{sort_target}/#{sort_order}" do
      Idol.all_idols.sort do |a, b| sort_handler(a, b) * (sort_order == :asc ? 1 : -1) end
    end

    # meta tags
    set_meta_tags twitter: {
      card: 'summary',
      site: '@tottokotkd',
      title: '全自動でツイート集計したやつ',
      description: '15分毎に自動更新するけど何回もアクセスしないで (サーバーが死ぬ)',
    }

  end

  def update
    begin
      SearchRecord.update
    rescue => e
      puts e
    end

    redirect_to action: 'index'
  end

  private

  def sort_target
    keys =%w(id name tweet account tw_p_a)
    keys.include?(params[:sort]) ? params[:sort].to_sym : :name
  end

  def sort_order
    params[:order].eql?('desc') ? :desc : :asc
  end

  def sort_handler(a, b)
    case sort_target
      when :name
        a.display_yomi <=> b.display_yomi
      when :tweet
        a.tweet_count <=> b.tweet_count
      when :account
        a.user_count <=> b.user_count
      when :tw_p_a
        a.tpu_count <=> b.tpu_count
      else
        a.id <=> b.id
    end
  end

  helper_method %i(sort_target sort_order)
end
