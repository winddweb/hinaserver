def time_range(start, last, interval)
  count = (last - start) / interval
  (0..count).map { |c| start + interval * c }
end
