require 'rails_helper'

RSpec.describe SearchRecord, type: :model do
  specify 'search_idol to return a correct idol' do
    idols = Idol.all
    record = SearchRecord.create!

    araki = record._find_idol idols, '【アイドルマスターシンデレラガールズ】で総選挙開催中!! 荒木比奈に投票したよ #シンデレラガール総選挙'
    expect(araki.display_name).to eq '荒木比奈'

    nao = record._find_idol idols, '【アイドルマスターシンデレラガールズ】でシンデレラガール総選挙開催中!! 神谷奈緒に投票しろ #シンデレラガール総選挙'
    expect(nao.display_name).to eq '神谷奈緒'

    retweet = record._find_idol idols, 'RT @tottokotkd: 【アイドルマスターシンデレラガールズ】で総選挙開催中!! 荒木比奈に投票したよ #シンデレラガール総選挙'
    expect(retweet).to be_nil
  end
end
