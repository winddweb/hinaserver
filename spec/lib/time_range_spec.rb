require 'rails_helper'
require 'util/time_range'

RSpec.describe 'lib/time_range' do
  specify 'time_range to create correct array' do
    start = Time.local 2017, 4, 10, 15
    last = Time.local 2017, 5, 9, 19
    interval = 15.minutes

    result = time_range(start, last, interval)
    expect(result.first).to eq start
    expect(result.last).to eq last
    result.reduce(start) do |expected, next_value|
      expect(next_value).to eq expected
      next_value + interval
    end
    expect(result.length).to eq 2801

  end
end
